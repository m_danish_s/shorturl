<?php

	require_once('BaseConverter.php');
	require_once('DatabaseHelper.php');
	/**
	* Contains static methods through which you can get the shortened url
	*/
	class URLShortner {
		
		public static function getShortUrl($longUrl) {

			if(strlen($longUrl) == 0) {
				return null;

			}else{

				$databaseHelper = new DatabaseHelper();
				$url = $databaseHelper->saveShortUrl($longUrl);
				return $url;
				
			}
		}

		public static function getLongUrl($shortUrl){

			/*
			I first thought I would decode the url and then
			fetch it with the id, but because now I have the shortened url
			in the db, so why not het the original one using that.
			*/

			$databaseHelper = new DatabaseHelper();
			$url = $databaseHelper->getLongUrl($shortUrl);

			if(strpos($url,'http://') === false){
				$url = 'http://' . $url;
			}

			return $url;
		}


	}
?>