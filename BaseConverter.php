<?php
	class BaseConverter{
		private static $alphabets = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

		public static function convertToBase62($number){
			$base = 62;
			$result = "";

			//divide the number by base and append the remainder
			while($number > 0){
				$result .= self::$alphabets[$number%$base];
				$number =  (int)($number/$base);
			}

			//resultant string is the reverse of remainders
			return strrev($result);

		}

		public static function convertToBase10($string){
			$base = 62;
			$power = 0;
			$result = 0;

			$index = strlen($string)-1;

			//multiply the index of each character by (base raised to the power of placevalue)
			//the sum of individal values is the resultant decimal number
			while ($index >= 0) {


				$pos = strpos(self::$alphabets, $string[$index]);

				$result += (($pos) * pow($base,$power) );
				$power++;
				$index--;
			}

			return $result;

		}
	}
?>