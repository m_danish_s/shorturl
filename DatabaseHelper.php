<?php
	require_once('BaseConverter.php');
	/**
	* A helper class used to store and retrieve data from the database
	*/
	class DatabaseHelper
	{
		private $database = "dbname";
		private $server = "servername";
		private $user = "username";
		private $password = "password";
		private $connection = null;

		private $getLongUrlQuery = "SELECT longurl FROM url WHERE shorturl = ?";
		private $saveLongUrlQuery = "INSERT INTO url (longurl) VALUES (?)";		
		private $updateLongUrl = "UPDATE url SET shorturl = ? WHERE id = ?";		

		//initialise a connection for this instance
		function __construct()
		{

			$this->connection = new mysqli($this->server,$this->user,$this->password,$this->database);

			if($this->connection->connect_errno){
				//handle any way you want
				echo "Oops, Something Went Wrong.";
				exit();
			}else{
				
			}
		}

		//returns the long url by mapping the short url in the db
		function getLongUrl($shortUrl){

			$stmt = $this->connection->prepare($this->getLongUrlQuery);
			$stmt->bind_param('s',$shortUrl);

			if($stmt->execute()){
				$stmt->bind_result($longUrl);
				$stmt->fetch();
				$stmt->close();
				return $longUrl;
				
			}else{
				return null;
			}
		}

		function saveShortUrl($longUrl){

			//something might go wrong between the two cycles to db
			//so handle it as a transaction
			if($this->connection->autocommit(FALSE)){
				
				//prepare and execute to save long url with default shorturl
				$stmt = $this->connection->prepare($this->saveLongUrlQuery);
				$stmt->bind_param('s',$longUrl);

				if($stmt->execute()){
					//get the id of the saved url
					$id = $this->connection->insert_id;

					//get the short version of the url
					$shortUrl = BaseConverter::convertToBase62($id);
					$stmt->close();

					//update the last inserted record with the short url
					$stmt = $this->connection->prepare($this->updateLongUrl);
					$stmt->bind_param('si',$shortUrl,$id);
					if($stmt->execute()){
						$stmt->close();
						$this->connection->commit();
						$this->connection->autocommit(true);
						return $shortUrl;
					}else{
						$this->connection->rollback();
					}
					
				}
			}
			$this->connection->autocommit(TRUE);
			return null;
			
		}

		function __destruct(){
			$this->connection->close();
		}
	}

?>