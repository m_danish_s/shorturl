<?php

require_once('URLShortner.php');

$shortUrl = "";

$uri = $_SERVER['REQUEST_URI'];

$uri = ltrim($uri,"/");

$code = explode("/", $uri);
$code = $code[1];


if(isset($_POST['longurl'])){
	$shortUrl = URLShortner::getShortUrl($_POST['longurl']);

}else if(isset($code) && !empty($code)){
	$url = URLShortner::getLongUrl($code);
	// echo 'location: '.$url;
	header('location: '.$url);
	

}

?>

<html>
<head>
	<title>Short URL</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>

	<h1>URL SHORTNER</h1>

	<div align="center" class="content">
		<form action="index.php" method="post">
			<input type="text" name="longurl">
			<input type="submit" class="orange-flat-button" value="submit">
		</form>

	
		<p class="url">
			<?php 
				if($shortUrl != ""){
					echo 'http://mdanish.info/shorturl/'.$shortUrl;	
				}
				
			?>
		</p>
	</div>

</body>

</html>